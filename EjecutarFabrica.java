package com.lfac.FabricaAnimales;

public class EjecutarFabrica {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FabricaAnimales fabrica;
		fabrica = new FabricaPatos();
		Animal pato = fabrica.crearAnimal();
		pato.emitirSonido();
		fabrica = new FabricaPerros();
		Animal perro = fabrica.crearAnimal();
		perro.emitirSonido();
	}

}
